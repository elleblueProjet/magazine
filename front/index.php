<?php  
	require 'function.php';
	$sports = get_all_table("article where categorie_id = 4");
	$tech = get_all_table("article where categorie_id = 2 limit 2");
	
	$all = get_all_table("article");
	$reste = sizeof($all)-3;
	$actualite = get_all_table("article order by (dateEntree) asc limit ".$reste);
	$dernierArticle = get_all_table("article order by (dateEntree) desc limit 3");

	$liste = get_all_table("article");
	$one = rand(1,sizeof($liste)-1);
	$two = rand(1,sizeof(get_liste_exclus($one)));
	$three = rand(1,sizeof(get_liste_exclus($two)));

	$url_one = "alaune/".get_reference($all[$one]['article_id'])."-".$all[$one]['article_id'];
	$url_two = "alaune/".get_reference($all[$two]['article_id'])."-".$all[$two]['article_id'];
	$url_three = "alaune/".get_reference($all[$three]['article_id'])."-".$all[$three]['article_id'];

	$sport1 = rand(0,sizeof($sports)); 
	$sport2 = rand(0,$sport1);

	$url_sport1 = "sports/".get_reference($all[$sport1]['article_id'])."-".$all[$sport1]['article_id'];
	$url_sport2 = "sports/".get_reference($all[$sport2]['article_id'])."-".$all[$sport2]['article_id'];
								
?>
<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | Accueil</title>
<?php include 'link.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="La nouvelle du jour est un site d'information comme son nom l'indique. Elle offre aux gens l'opportunité de suivre les nouvelles n'importe où et ailleur." />
<meta name="keywords" content="Nouvelle du jour,sport,technologie,celebrite,film,musique,actualite,nouvelle,news,magazine" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


</head>
<body>
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<!-- script for menu -->
			<div class="clearfix"></div>
			<div class="main-content">		
				<div class="col-md-9 total-news">
				<div class="posts">
					<div class="left-posts">
						<div class="world-news">
							<div class="main-title-head">
								<h3>dans le monde entier</h3>
								<div class="clearfix"></div>
							</div>
							<div class="world-news-grids">
								<?php for ($i=0; $i < 3; $i++) { 
										$indices = rand($i,sizeof($actualite)-1);
										$url_actualite = "actualite/".get_reference($actualite[$i]['article_id'])."-".$actualite[$i]['article_id'];
									?>

									<div class="world-news-grid" style="height: 300px">
										<img src="<?php echo $actualite[$indices]['image']; ?>" alt="<?php echo formate_caractere_speciaux($actualite[$indices]['titre']); ?>" />
										<a href="<?php echo $url_actualite; ?>" class="title"><?php echo formate_caractere_speciaux($actualite[$indices]['titre']); ?></p>
										<a href="<?php echo $url_actualite; ?>">Lire la suite</a>
									</div>
								<?php } ?>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="latest-articles">
							<div class="main-title-head">
								<h3>les derniers articles</h3>
								<div class="clearfix"></div>
							</div>
							<div class="world-news-grids">
								<?php for ($i=0; $i < sizeof($dernierArticle); $i++) { 
									$urlDernierArticle = "dernier-article/".get_reference($dernierArticle[$i]['article_id'])."-".$dernierArticle[$i]['article_id'];
									?>
									
									<div class="world-news-grid" style="height: 300px">
										<img src="<?php echo $dernierArticle[$i]['image']; ?>" alt="" />
										<a href="<?php echo $urlDernierArticle; ?>" class="title"><?php echo formate_caractere_speciaux($dernierArticle[$i]['titre']); ?></p>
										<a href="<?php echo $urlDernierArticle; ?>">Lire la suite</a>
									</div>
								<?php } ?>
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="tech-news">
							<div class="main-title-head">
								<h3>la technologie</h3>
								<a href="technologie">Plus  +</a>
								<div class="clearfix"></div>
							</div>	
							<div class="tech-news-grids">
								<?php for ($i=0; $i<sizeof($tech); $i++) { 
										$url_tech = "technologie/".get_reference($tech[$i]['article_id'])."-".$tech[$i]['article_id'];
									?>
									<div class="col-md-6">
										<div class="tech-news-grid">
											<a href="<?php echo $url_tech; ?>"><?php echo formate_caractere_speciaux($tech[$i]['titre']); ?></a>
											<p><?php echo formate_caractere_speciaux($tech[$i]['description']); ?></p>
											<p>par<a href="#"> Admin </a>  </p>
										</div>
									</div>
								<?php } ?>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="right-posts">
						<div class="desk-grid">
							<h3>a la une</h3>
							<div class="desk">
								<a href="<?php echo $url_one; ?>" class="title"><?php echo formate_caractere_speciaux($all[$one]['titre']); ?></a>
								<p><?php echo formate_caractere_speciaux($all[$one]['description']); ?></p>
								<p><a href="<?php echo $url_one; ?>">Lire la suite</a></p>
							</div>
							<div class="desk">
								<a href="<?php echo $url_two; ?>" class="title"><?php echo formate_caractere_speciaux($all[$two]['titre']); ?></a>
								<p><?php echo formate_caractere_speciaux($all[$two]['description']); ?></p>
								<p><a href="<?php echo $url_two; ?>">Lire la suite</a></p>
							</div>
							<div class="desk">
								<a href="<?php echo $url_three; ?>" class="title"><?php echo formate_caractere_speciaux($all[$three]['titre']); ?></a>
								<p><?php echo formate_caractere_speciaux($all[$three]['description']); ?></p>
								<p><a href="<?php echo $url_three; ?>">Lire la suite</a></p>
							</div>
						</div>
						<div class="editorial">
							<h3>sports</h3>
							<a href="sports">Plus  +</a>
							<div class="editor">
								<a href="<?php echo $url_sport1; ?>"><img src="<?php echo $sports[$sport1]['image']; ?>" alt="<?php echo formate_caractere_speciaux($sports[$sport1]['titre']); ?>" /></a>
								<a href="<?php echo $url_sport1; ?>"><?php echo formate_caractere_speciaux($sports[$sport1]['titre']); ?></a>
							</div>
							<div class="editor">
								<a href="<?php echo $url_sport2; ?>"><img src="<?php echo $sports[$sport2]['image']; ?>" alt="<?php echo formate_caractere_speciaux($sports[$sport2]['titre']); ?>" /></a>
								<a href="<?php echo $url_sport2; ?>"><?php echo formate_caractere_speciaux($sports[$sport2]['titre']); ?></a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>	
				</div>
				</div>
				<div class="col-md-3 side-bar">
					<?php include 'films_actualite.php'; ?>
				<div class="clearfix"></div>

					<div class="popular mpopular">
							<?php include 'populaire.php'; ?>
					</div>
				<div class="clearfix"></div>
				</div>	
				<div class="clearfix"></div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>