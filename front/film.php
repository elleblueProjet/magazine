<?php  
	require 'function.php';
	$film = get_all_table("article where categorie_id = 3");
?>
<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | Film</title>
<?php include 'link.php'; ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="La nouvelle du jour est un site d'information comme son nom l'indique. Elle offre aux gens l'opportunité de suivre les nouvelles n'importe où et ailleur." />
<meta name="keywords" content="Nouvelle du jour,sport,technologie,celebrite,film,musique,actualite,nouvelle,news,magazine" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->

</head>
<body>
	
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<div class="clearfix"></div>
			<div class="main-content">		
				<div class="col-md-9 total-news">
							<div class="world-news-grids">
								<?php for ($i=0; $i < sizeof($film); $i++) { 
$url_film = "film/".get_reference($film[$i]['article_id'])."-".$film[$i]['article_id'];
									?>
									<div class="world-news-grid" style="height: 300px">
										<a href="<?php echo $url_film; ?>"><img src="<?php echo $film[$i]['image']; ?>" alt="<?php echo formate_caractere_speciaux($film[$i]['titre']); ?>" /></a>
										<a href="<?php echo $url_film; ?>" class="title"><?php echo formate_caractere_speciaux($film[$i]['titre']); ?></p>
										<a href="singlepage.php?idarticle=<?php echo $film[$i]['article_id']; ?>">Lire la suite</a>
									</div>
								<?php } ?>
								<div class="clearfix"></div>
							</div>
					<div class="clearfix"></div>	
				</div>
				</div>	
				<div class="col-md-3 side-bar">
					<div class="popular mpopular">
						<?php include 'populaire.php'; ?>
					</div>
					<div class="clearfix"></div>

				</div>	
				<?php include 'autreArticle.php'; ?>
				<div class="clearfix"></div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>