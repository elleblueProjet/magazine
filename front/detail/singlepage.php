<?php  
date_default_timezone_set('Europe/Bucharest');
	require '../function.php';
	$fiche = get_all_table("article where article_id = '".$_GET['idarticle']."' order by(dateEntree) desc");
		
	$daty = date_fr($fiche[0]['dateEntree']);
$liste = get_all_table("article where article_id != '".$_GET['idarticle']."'");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | <?php echo $fiche[0]['titre']; ?></title>
<?php include '../link.php'; ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="The News Reporter Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->

</head>
<body>
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<div class="clearfix"></div>
			<div class="blog-main-content">		
				<div class="col-md-9 total-news">
					<div class="grids">
						<div class="grid box">
							<div class="grid-header">
								<h1 class="text-center"><b><?php echo get_phrase($fiche[0]['titre']); ?></h1></b><br>
								<h2><?php echo $fiche[0]['description']; ?></h2>
								<ul>
								<li><span>Posté par<a href="#"> Admin</a> le <?php echo $daty; ?> </span></li>
								</ul>
							</div>
							<div class="singlepage">
								<div class="col-md-12">
									<a href="#"><img src="<?php echo $fiche[0]['image']; ?>" /></a>
									<?php echo $fiche[0]['article']; ?><br>
								</div>
							</div>
						</div>
					</div>
				</div>	
				
				<div class="col-md-3 side-bar">
					<?php include '../films_actualite.php'; ?>
				<div class="clearfix"></div>

					<div class="popular mpopular">
							<?php include '../populaire.php'; ?>
					</div>
				<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php include '../footer.php'; ?>
		</div>
	</div>
</body>
</html>