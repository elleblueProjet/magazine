<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | A Propos de nous</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<?php include 'link.php'; ?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="La nouvelle du jour est un site d'information comme son nom l'indique. Elle offre aux gens l'opportunité de suivre les nouvelles n'importe où et ailleur." />
<meta name="keywords" content="Nouvelle du jour,sport,technologie,celebrite,film,musique,actualite,nouvelle,news,magazine" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

</head>
<body>
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<div class="clearfix"></div>
			<div class="main-content">		
				<div class="about-section">
					<div class="about-us">
						<div class="col-md-7 about-left">
							<h3>UN PETIT MOT A PROPOS DE NOUS</h3>
							<div class="abt_image">
								<img src="images/abt_pic.jpg" alt="" />
							</div>
							<h5>Lorem ipsum eos accusamu dolore massa lore fugharum quidemed rerum faciliseme iusto ssimos ducimus.</h5>
							<p>Ses praesentiumvoluptatum delenitimos atqcorrupti quos dolores et quas molestias exceuri occaecati cupiditate non prdent imilique.</p>
							<p>Sunt in culpaqui officia mos deserunt mollitia animid est laborum dolorum fuharumos.Ses praesentiumvoluptatum delenitimos atqcorrupti quos dolores et quas molestias exceuri occaecati cupiditate non prdent imiliqueunt in culpaqui officia mos deserunt mollitia animid est laborum dolorum fuharumosiemen quidemed. Rerumol facilisest et expedita distinc.</p>
							<p>Nam libero temprecum soluta nobis est eligendi optio cumquenihil perspic iatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit.</p>
						</div>
						<div class="col-md-5 about-right">
							<h3>CE QU'ON OFFRE</h3>
							<div class="offer">
								<h4>1</h4>
								<a href="#">Les nouvelles dans le monde entier</a>
								<div class="clearfix"></div>
								<p>Ut enim ad minim veniam, quis nostrud exercitat ion ullamcode laboris nisi dolore massa ealiquipx eato commodo consectetuor massa perspiciatis unde omnis iste natus error sit.</p>
							</div>
							<div class="offer">
								<h4>2</h4>
								<a href="#">PRAESENT VESTIBULUM MOLESTIE LACUS</a>
								<div class="clearfix"></div>
								<p>Ut enim ad minim veniam, quis nostrud exercitat ion ullamcode laboris nisi dolore massa ealiquipx eato commodo consectetuor massa perspiciatis unde omnis iste natus error sit.</p>
							</div>
							<div class="offer">
								<h4>3</h4>
								<a href="#">SED UT PERSPICIATIS UNDE OMNIS ISTE</a>
								<div class="clearfix"></div>
								<p>Ut enim ad minim veniam, quis nostrud exercitat ion ullamcode laboris nisi dolore massa ealiquipx eato commodo consectetuor massa perspiciatis unde omnis iste natus error sit.</p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="team">
						<h3 class="text-center">Membre</h3>
						<div class="team-grids text-center">
							<div class="col-md-12 team-grid">
								<img src="lova.jpg" alt="" width="160px" height="200px" />
								<h5><b>RANDRIAMANDIMBITIANA</b> <u><b>Lova</b></u>nirina Murelle</h4>
								<p>ETU000523, Promotion 9, Groupe A</p>
							</div>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>