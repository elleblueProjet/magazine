<?php  
	require 'function.php';
	$key = $_GET['motCle'];
	$result = get_search_key($key);
?>
<style type="text/css">
	.panel_result{
		box-shadow: 0.5px 0.05em 0.5px #B5B7BD;
		background: white;
		padding: 5px 5px 5px 20px; 
		margin-top: 10px;
		margin-bottom: 10px;
	}
	h3{
		text-align: center;
		font-weight: bold;
	}
	.lien{
		color: #677EF0;
	}
	.lien:hover{
		color: #CF5555;
		text-decoration: underline white;
	}
</style>
<div class="main-title-head">
	<h3>Le resultat a retourne <?php echo sizeof($result); ?> resultats</h3>
	<div class="clearfix"></div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<?php for ($i=0; $i < sizeof($result); $i++) { 
		$url = "detail/".get_reference($result[$i]['article_id'])."-".$result[$i]['article_id']."".".html";
		$daty = date_fr($result[$i]['dateEntree']);
			?>
			<div class="panel_result">
				<a href="<?php echo $url; ?>" ><h3 class="lien"><?php echo formate_caractere_speciaux($result[$i]['titre']); ?></h3></a>
				<h5 class="text-center"><?php echo formate_caractere_speciaux($result[$i]['description']); ?></h5><br>
				<i><h6 class="text-right">Ajouté le <?php echo $daty; ?></h6></i>
			</div>
		<?php } ?>
	</div>
</div>