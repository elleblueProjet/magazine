<?php  
	require 'function.php';
	$celeb = get_all_table("article where categorie_id = 5");
?>
<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | Célébrité</title>
<?php include 'link.php'; ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="La nouvelle du jour est un site d'information comme son nom l'indique. Elle offre aux gens l'opportunité de suivre les nouvelles n'importe où et ailleur." />
<meta name="keywords" content="Nouvelle du jour,sport,technologie,celebrite,film,musique,actualite,nouvelle,news,magazine" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->

</head>
<body>
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<div class="clearfix"></div>
			<div class="main-content">		
				<div class="col-md-9 total-news">
					<div class="world-news-grids">
						<?php for ($i=0; $i < sizeof($celeb); $i++) { 
$url_celeb = "celebrite/".get_reference($celeb[$i]['article_id'])."-".$celeb[$i]['article_id'];
							?>
							<div class="world-news-grid" style="height: 300px">
								<a href="<?php echo $url_celeb; ?>"><img src="<?php echo $celeb[$i]['image']; ?>" alt="<?php echo formate_caractere_speciaux($celeb[$i]['titre']); ?>" /></a>
								<a href="<?php echo $url_celeb; ?>" class="title"><?php echo formate_caractere_speciaux($celeb[$i]['titre']); ?>
								<a href="singlepage.php?idarticle=<?php echo $celeb[$i]['article_id']; ?>">Lire la suite</a>
							</div>
						<?php } ?>
					<div class="clearfix"></div>
					</div>
				</div>	
				<div class="col-md-3 side-bar">
					<?php include 'films_actualite.php'; ?>
				<div class="clearfix"></div>

					<div class="popular mpopular">
							<?php include 'populaire.php'; ?>
					</div>
				<div class="clearfix"></div>
				</div>	
				<?php include 'autreArticle.php'; ?>
				<div class="clearfix"></div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>