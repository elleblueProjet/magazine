<?php  
	require 'function.php';	
?>
<!DOCTYPE html>
<html>
<head>
<title>La nouvelle du jour | Recherche</title>
<?php include 'link.php'; ?>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="La nouvelle du jour est un site d'information comme son nom l'indique. Elle offre aux gens l'opportunité de suivre les nouvelles n'importe où et ailleur." />
<meta name="keywords" content="Nouvelle du jour,sport,technologie,celebrite,film,musique,actualite,nouvelle,news,magazine" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
<body>
	<script type="text/javascript">
		function recherche() {
		    $.get("rechercheResult.php?motCle="+$('#motCherche').val(),function(rep) {
		        $(".resultat").html(rep);
		    });
		}
	</script>
	<!-- header-section-starts -->
	<div class="container">	
		<div class="news-paper">
			<?php include 'header.php'; ?>
			<?php include 'menu.php'; ?>
			<!-- script for menu -->
			<div class="clearfix"></div>
			<div class="main-content">		
				<div class="col-md-9 total-news">
					<div class="col-md-12">
						<div class="form-group">
		<input type="text" id="motCherche" onkeyup="recherche()" class="form-control" placeholder="Mot à chercher">
						</div>
					</div>
					
					<div class="resultat">
						
					</div>
				</div>
				<div class="col-md-3 side-bar">
					<?php include 'films_actualite.php'; ?>
					<div class="clearfix"></div>

					<div class="popular mpopular">
						<?php include 'populaire.php'; ?>
					</div>
					<div class="clearfix"></div>
				</div>	
				<div class="clearfix"></div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
	</div>
	
</body>
</html>

