<?php  
	require 'function.php';
	$mot = $_POST['motCle'];
	$cat = get_all_table("categorie");
	
	if($mot == "recherche_simple"){ ?>
		<div class="col-md-offset-3 col-md-6 col-md-offset-3">
			<div class="form-group">
				<input type="text" id="motCherche" onKeyup="recherche()" class="form-control" placeholder="Mot à chercher">
			</div>
		</div>
	<?php }else{ ?>
		<div class="col-md-offset-2 col-md-6">
			<div class="form-group">
				<label>Sélectionner la catégorie de votre article</label>
				<select id="categorie" class="form-control">
					<?php for ($i=0; $i < sizeof($cat); $i++) { ?>
						<option value="<?php echo $cat[$i]['categorie_id']; ?>"><?php echo $cat[$i]['categorie']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	<?php }
?>