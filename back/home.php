<?php  
	require 'function.php';
	$count_musique = get_count_function(" article where categorie_id = 1");
	$count_tech = get_count_function(" article where categorie_id = 2");
	$count_film = get_count_function(" article where categorie_id = 3");
	$count_sports = get_count_function(" article where categorie_id = 4");
	$count_celebrite = get_count_function(" article where categorie_id = 5");
	$count_all = get_count_function(" article");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Back office de La nouvelle du jour- Acceuil</title>
	<?php include 'include.php'; ?>
</head>
<body>
	<?php include 'nav_bar.php'; ?>
	<?php include 'menu.php'; ?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Acceuil</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Acceuil</h1>
			</div>
		</div><!--/.row-->
		
		
			<?php include 'statistique.php'; ?>
		
	</div>	<!--/.main-->
	<?php include 'script.php'; ?>
</body>
</html>