<style type="text/css">
	.card{
		margin-top: 5px;
		margin-bottom: 5px;
		box-shadow: 1px 0px 0px 1px #E3E3E3;
	}
</style>
<div class="panel panel-container">
	<div class="row">
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-teal panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-music color-blue"></em>
					<div class="large"><?php echo $count_musique; ?></div>
					<div class="text-muted">Articles de musiques</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-blue panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-laptop color-orange"></em>
					<div class="large"><?php echo $count_tech; ?></div>
					<div class="text-muted">Articles sur la technologie</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-orange panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-film color-teal"></em>
					<div class="large"><?php echo $count_film; ?></div>
					<div class="text-muted">Articles sur les films</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-red panel-widget ">
				<div class="row no-padding"><em class="fa fa-xl fa-dribbble color-red"></em>
					<div class="large"><?php echo $count_sports; ?></div>
					<div class="text-muted">Articles sur les sports</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-red panel-widget ">
				<div class="row no-padding"><em class="fa fa-xl fa-users color-red"></em>
					<div class="large"><?php echo $count_celebrite; ?></div>
					<div class="text-muted">Articles sur la célébrité</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
			<div class="panel panel-red panel-widget ">
				<div class="row no-padding"><em class="fa fa-xl fa-line-chart color-red"></em>
					<div class="large"><?php echo $count_all; ?></div>
					<div class="text-muted">Toutes les articles</div>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>

