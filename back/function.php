<?php 
	function connect(){
		$user='id4517803_magazine';
		$pass='magazine';
		$dsn='mysql:host=localhost;port=3306;dbname=id4517803_magazine';
		$connexion = new PDO($dsn, $user, $pass);
		return $connexion;
	}

	function get_all_table($table){
		$sql="SELECT * FROM ".$table;
		$res = connect()->query($sql);
		$contenu = array();		
		while($donnee = $res->fetch(PDO::FETCH_ASSOC)){
			$contenu[] = $donnee;
		}
			$res->closeCursor();
		    return $contenu;
		    mysql_close(connect());		
	}
	function get_search_key($mot){
		$sql="SELECT * FROM article where titre like '%".$mot."%' or description like '%".$mot."%' or article like '%".$mot."%'";
		$res = connect()->query($sql);
		$contenu = array();		
		while($donnee = $res->fetch(PDO::FETCH_ASSOC)){
			$contenu[] = $donnee;
		}
			$res->closeCursor();
		    return $contenu;
		    mysql_close(connect());		
		
	}

	function get_phrase($mot){
		return str_replace("'", "\\", $mot);
	}

	function formate_reference($ref){
    $ref= preg_replace("/(!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_)/","-",$ref); 
    	$ref =  preg_replace("/(à)/","a",$ref);  
    	$ref =  preg_replace("/(é|è|ê|É)/","e",$ref);  
    	$ref =  preg_replace("/(î|ï)/","i",$ref);  
    	$ref =  preg_replace("/(ç)/","c",$ref);    
    	return strtolower($ref);
    }

    function formate_caractere_speciaux($text){
    	$text = preg_replace("/(¡)/","&iexcl;",$text);
		$text = preg_replace("/(¢)/","&cent;",$text);
		$text = preg_replace("/(£)/","&pound;",$text);
		$text = preg_replace("/(¤)/","&curren;",$text);
		$text = preg_replace("/(¥)/","&yen;",$text);
		$text = preg_replace("/(¦)/","&brvbar;",$text);
		$text = preg_replace("/(§)/","&sect;",$text);
		$text = preg_replace("/(¨)/","&uml;",$text);
		$text = preg_replace("/(©)/","&copy;",$text);
		$text = preg_replace("/(ª)/","&ordf;",$text);
		$text = preg_replace("/(«)/","&laquo;",$text);
		$text = preg_replace("/(¬)/","&not;",$text);
		$text = preg_replace("/(®)/","&reg;",$text);
		$text = preg_replace("/(¯)/","&masr;",$text);
		$text = preg_replace("/(°)/","&deg;",$text);
		$text = preg_replace("/(±)/","&plusmn;",$text);
		$text = preg_replace("/(²)/","&sup2;",$text);
		$text = preg_replace("/(³)/","&sup3;",$text);
		$text = preg_replace("/(')/","&acute;",$text);
		$text = preg_replace("/(µ)/","&micro;",$text);
		$text = preg_replace("/(¶)/","&para;",$text);
		$text = preg_replace("/(·)/","&middot;",$text);
		$text = preg_replace("/(¸)/","&cedil;",$text);
		$text = preg_replace("/(¹)/","&sup1;",$text);
		$text = preg_replace("/(º)/","&ordm;",$text);
		$text = preg_replace("/(»)/","&raquo;",$text);
		$text = preg_replace("/(¼)/","&frac14;",$text);
		$text = preg_replace("/(½)/","&frac12;",$text);
		$text = preg_replace("/(¾)/","&frac34;",$text);
		$text = preg_replace("/(¿)/","&iquest;",$text);
		$text = preg_replace("/(À)/","&Agrave;",$text);
		$text = preg_replace("/(Á)/","&Aacute;",$text);
		$text = preg_replace("/(Â)/","&Acirc;",$text);
		$text = preg_replace("/(Ã)/","&Atilde;",$text);
		$text = preg_replace("/(Ä)/","&Auml;",$text);
		$text = preg_replace("/(Å)/","&Aring;",$text);
		$text = preg_replace("/(æ)/","&Aelig;",$text);
		$text = preg_replace("/(Ç)/","&Ccedil;",$text);
		$text = preg_replace("/(È)/","&Egrave;",$text);
		$text = preg_replace("/(É)/","&Eacute;",$text);
		$text = preg_replace("/(Ê)/","&Ecirc;",$text);
		$text = preg_replace("/(Ë)/","&Euml;",$text);
		$text = preg_replace("/(Ì)/","&Igrave;",$text);
		$text = preg_replace("/(Í)/","&Iacute;",$text);
		$text = preg_replace("/(Î)/","&Icirc;",$text);
		$text = preg_replace("/(Ï)/","&Iuml;",$text);
		$text = preg_replace("/(Ð)/","&eth;",$text);
		$text = preg_replace("/(Ñ)/","&Ntilde;",$text);
		$text = preg_replace("/(Ò)/","&Ograve;",$text);
		$text = preg_replace("/(Ó)/","&Oacute;",$text);
		$text = preg_replace("/(Ô)/","&Ocirc;",$text);
		$text = preg_replace("/(Õ)/","&Otilde;",$text);
		$text = preg_replace("/(Ö)/","&Ouml;",$text);
		$text = preg_replace("/(×)/","&times;",$text);
		$text = preg_replace("/(Ø)/","&Oslash;",$text);
		$text = preg_replace("/(Ù)/","&Ugrave;",$text);
		$text = preg_replace("/(Ú)/","&Uacute;",$text);
		$text = preg_replace("/(Û)/","&Ucirc;",$text);
		$text = preg_replace("/(Ü)/","&Uuml;",$text);
		$text = preg_replace("/(Ý)/","&Yacute;",$text);
		$text = preg_replace("/(Þ)/","&thorn;",$text);
		$text = preg_replace("/(ß)/","&szlig;",$text);
		$text = preg_replace("/(à)/","&agrave;",$text);
		$text = preg_replace("/(á)/","&aacute;",$text);
		$text = preg_replace("/(â)/","&acirc;",$text);
		$text = preg_replace("/(ã)/","&atilde;",$text);
		$text = preg_replace("/(ä)/","&auml;",$text);
		$text = preg_replace("/(å)/","&aring;",$text);
		$text = preg_replace("/(æ)/","&aelig;",$text);
		$text = preg_replace("/(ç)/","&ccedil;",$text);
		$text = preg_replace("/(è)/","&egrave;",$text);
		$text = preg_replace("/(é)/","&eacute;",$text);
		$text = preg_replace("/(ê)/","&ecirc;",$text);
		$text = preg_replace("/(ë)/","&euml;",$text);
		$text = preg_replace("/(ì)/","&igrave;",$text);
		$text = preg_replace("/(í)/","&iacute;",$text);
		$text = preg_replace("/(î)/","&icirc;",$text);
		$text = preg_replace("/(ï)/","&iuml;",$text);
		$text = preg_replace("/(ð)/","&eth;",$text);
		$text = preg_replace("/(ñ)/","&ntilde;",$text);
		$text = preg_replace("/(ò)/","&ograve;",$text);
		$text = preg_replace("/(ó)/","&oacute;",$text);
		$text = preg_replace("/(ô)/","&ocirc;",$text);
		$text = preg_replace("/(õ)/","&otilde;",$text);
		$text = preg_replace("/(ö)/","&ouml;",$text);
		$text = preg_replace("/(÷)/","&divide;",$text);
		$text = preg_replace("/(ø)/","&oslash;",$text);
		$text = preg_replace("/(ù)/","&ugrave;",$text);
		$text = preg_replace("/(ú)/","&uacute;",$text);
		$text = preg_replace("/(û)/","&ucirc;",$text);
		$text = preg_replace("/(ü)/","&uuml;",$text);
		$text = preg_replace("/(ý)/","&yacute;",$text);
		$text = preg_replace("/(þ)/","&thorn;",$text);
		$text = preg_replace("/(ÿ)/","&yuml;",$text);
		$text = preg_replace("/(ÿ)/","&yuml;",$text);
		get_phrase($text);
		return $text;
	}

    

	function insert_article($categorie,$titre,$desc,$article,$image){
		$reference = formate_reference($titre);
		$sql="insert into article (categorie_id,titre,description,article,dateEntree,image,reference) values('%s','%s','%s','%s',CURDATE(),'%s','".$reference."')";
		$requete = sprintf($sql,$categorie,formate_caractere_speciaux($titre),formate_caractere_speciaux($desc),formate_caractere_speciaux($article),$image);
		//echo $requete;
		connect()->exec($requete); 
	}
	function get_count_function($condition){
		$requete = "select count(*) as nb from ".$condition;
        $resultats = connect()->query($requete);
        $resultats->setFetchMode(PDO::FETCH_NUM);
        $nombre = 0;
		while($donnees = $resultats->fetch()){
			    $nombre = $donnees[0];	
		}
        $resultats->closeCursor();
        return $nombre;
    }
    
	
 ?>