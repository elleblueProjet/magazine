<?php  
	require 'function.php';
	$cat = get_all_table("categorie");
?>
<!DOCTYPE html>
<html>
<head>
	<title>La nouvelle du jour</title>
	<?php include 'include.php'; ?>
</head>
<body>
	<style type="text/css">
		a{
			cursor: pointer;
		}
	</style>
	<?php include 'nav_bar.php'; ?>
	<?php include 'menu.php'; ?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="home.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Mes articles</li>
			</ol>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Gérer les articles</h1>
			</div>
		</div>

		<div class="row">

			<div class="col-md-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
						Un nouvel article
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label>Dans quelle catégorie</label>
							<select class="form-control" id="categorie">
								<?php for ($i=0; $i < sizeof($cat); $i++) { ?>
									<option value="<?php echo $cat[$i]['categorie_id']; ?>"><?php echo strtoupper($cat[$i]['categorie']); ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
                       		<input type="text" id="titre" class="form-control" placeholder="Le titre de votre article">
                       	</div>
                       	<div class="form-group">
                       		<input type="text" id="description" class="form-control" placeholder="Une brève description">
                       	</div>
                       	<div class="form-group">
                       		<label>Image associé</label>
                       		<input type="file" onchange="previewFile()"><br>
                       		<div id="visualisation"></div>
                       	</div>
                       	<div class="form-group">
                       		<label>Intgralité de votre article</label>
                       		<textarea cols="40" rows="5" id="article" class="ckeditor"></textarea><br>
                       	</div>
                       	
                        <div class="form-group text-center">
                        	<a class="btn-lg btn-danger" onclick="insert_article()"> Insérer article</a>
                        </div>
                        <div id="erreur"></div>
					</div>
				</div>
			</div>
		</div>
			
	</div>	
	<?php include 'script.php'; ?>
</body>
</html>
