<?php  
	require 'function.php';
	$key = $_GET['motCle'];
	$result = get_search_key($key);
?>
<style type="text/css">
	.panel_result{
		box-shadow: 0.5px 0 0.5px #D3D5DB;
		background: white;
		padding: 5px 5px 5px 20px; 
		margin-top: 10px;
		margin-bottom: 10px;
	}
	h3{
		text-align: center;
		font-weight: bold;
	}
	.lien{
		color: #677EF0;
	}
	.lien:hover{
		color: #CF5555;
		text-decoration: underline white;
	}
</style>
<div class="row">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<?php for ($i=0; $i < sizeof($result); $i++) { ?>
			<div class="panel_result">
				<a href="" ><h3 class="lien"><?php echo formate_caractere_speciaux($result[$i]['titre']); ?></h3></a>
				<h5 class="text-center"><?php echo formate_caractere_speciaux($result[$i]['description']); ?></h5>
				<h6 class="text-right">Ajouté le <?php echo $result[$i]['dateEntree']; ?></h6>
			</div>
		<?php } ?>
	</div>
</div>