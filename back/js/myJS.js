function recherche(argument) {
    $.get("recherche.php?motCle="+$('#motCherche').val(),function(rep) {
        $(".resultat").html(rep);
    });
}
function previewFile() {
    var zoneImage = document.getElementById('visualisation');
    $('#visualisation').empty();
    var image = document.createElement('img')
                        
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();
                        
    reader.addEventListener("load", function () {
        image.src = reader.result;
        zoneImage.appendChild(image);
    }, false);
    if (file) {
        reader.readAsDataURL(file);
    }
}


function insert_article() {
    var categories = $('#categorie').val();
    var titres = $('#titre').val();
    var descs = $('#description').val();
    var articles = CKEDITOR.instances['article'].getData();
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();
        reader.addEventListener("load", function () {               
            var affiche = reader.result;
            var data = {categorie : categories ,titre : titres ,desc : descs ,article : articles ,image : affiche};
            $.ajax({
                url:"insert_article.php",
                method:"POST",
                data:{categorie : categories ,titre : titres ,desc : descs ,article : articles ,image : affiche},
              success: function(data){
                document.location.href = "articles.php";
                //$('#erreur').html(data);
              },
              error: function(xhr, ajaxOptions, thrownError) {
                   if (xhr.status === 200) {
                       console.log(ajaxOptions);
                   }
                   else {
                       console.log(xhr.status);
                       console.log(thrownError);
                   }
               }
            });
        }, false);
            if (file) {
                reader.readAsDataURL(file);
            }
}

function verify_connexion() {
    var emails = $('#mail').val();
    var mdps = $('#mdp').val();
    $.ajax({
        url:"verification_admin.php",
        method:"POST",
        data:{email:emails,mdp:mdps},
            success: function(data){
                $('#erreur').html(data);
              },
            error: function(xhr, ajaxOptions, thrownError) {
                   if (xhr.status === 200) {
                       console.log(ajaxOptions);
                   }
                   else {
                       console.log(xhr.status);
                       console.log(thrownError);
                   }
               }
            });
}

function search_simple() {
    $.ajax({
        url:"choix.php",
        method:"POST",
        data:{motCle:"recherche_simple"},
            success: function(data){
                $('.resultat').html('');
                $('.choix').html(data);
              },
            error: function(xhr, ajaxOptions, thrownError) {
                   if (xhr.status === 200) {
                       console.log(ajaxOptions);
                   }
                   else {
                       console.log(xhr.status);
                       console.log(thrownError);
                   }
               }
            });
}
function search_cat() {
    $.ajax({
        url:"choix.php",
        method:"POST",
        data:{motCle:"recherche_categorie"},
            success: function(data){
                $('.resultat').html('');
                $('.choix').html(data);
              },
            error: function(xhr, ajaxOptions, thrownError) {
                   if (xhr.status === 200) {
                       console.log(ajaxOptions);
                   }
                   else {
                       console.log(xhr.status);
                       console.log(thrownError);
                   }
               }
        });
}

