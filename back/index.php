<!DOCTYPE html>
<html>
<head>
	<?php include 'include.php'; ?>
	<title>La nouvelle du jour - Login</title>
	
</head>
<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Pseudonyme" id="mail" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Mot de passe" id="mdp" type="password" value="">
							</div>
							<a onClick="verify_connexion()" class="btn btn-danger">Se connecter</a>
						</fieldset>
					</form>
				</div>
			</div>
			<div id="erreur">
				
			</div>
 
		</div><!-- /.col-->
	</div><!-- /.row -->	
<?php include 'script.php'; ?>

</body>
</html>
