<?php  
	require 'function.php';
	$cat = get_all_table("categorie");
	$art = get_all_table("article");
?>
<!DOCTYPE html>
<html>
<head>
	<title>La nouvelle du jour</title>
	<?php include 'include.php'; ?>
</head>
<body>
	<style type="text/css">
		a{
			cursor: pointer;
		}
	</style>
	<?php include 'nav_bar.php'; ?>
	<?php include 'menu.php'; ?>
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="home.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Modifier</li>
			</ol>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Gérer les articles</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<a onClick="search_simple()" class="btn btn-warning">Chercher avant modification</a>
				</div>
				<div class="col-md-6">
					<a onClick="search_cat()" class="btn btn-warning">Chercher dans les catégories</a>
				</div>
			</div>
			<div class="choix" style="margin-top: 50px;">
			</div>
			<div class="resultat">
				
			</div>
		</div>
			
	</div>	
	<?php include 'script.php'; ?>
</body>
</html>
