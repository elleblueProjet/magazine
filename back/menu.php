<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Lovanirina</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>En ligne</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="acceuil.html"><em class="fa fa-home">&nbsp;</em> Acceuil</a></li>
			<li><a href="ajouter-un-article.html"><em class="fa fa-plus-circle">&nbsp;</em> Ajouter un article</a></li>
			<li><a href="gerer-votre-article-a-tout-moment.html"><em class="fa fa-edit">&nbsp;</em> Gérer mes articles </a></li>
		</ul>
	</div><!--/.sidebar-->